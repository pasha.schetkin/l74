<div id="comment-{{$comment->id}}">
    <div class="media g-mb-30 media-comment">
        <img class="d-flex g-width-50 g-height-50 rounded-circle g-mt-3 g-mr-15"
             src="{{asset('default_avatar.png')}}" alt="Image Description">
        <div class="media-body u-shadow-v18 g-bg-secondary g-pa-30">
            <div class="g-mb-15">
                @can('delete', $comment)
                    <button data-comment-id="{{$comment->id}}" type="button" class="btn btn-sm btn-outline-danger delete-comment">X</button>
                @endcan
                <h5 class="h5 g-color-gray-dark-v1 mb-0">{{$comment->user->name}}</h5>
                <span class="g-color-gray-dark-v4 g-font-size-12">{{$comment->created_at->diffForHumans()}}</span>
            </div>
            <p>

                {{$comment->body}}
            </p>
        </div>
    </div>
</div>
