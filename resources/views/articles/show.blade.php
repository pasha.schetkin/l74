@extends('layouts.app')
@section('content')

    @if (session('error'))
        <div class="row">
            <div class="col">
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col">
            <h3 class="article-title">
                {{$article->title}}
                <form style="display: inline-block;" action="{{route('articles.destroy', compact('article'))}}"
                      method="post">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-sm btn-outline-danger">Remove article</button>
                </form>
                | <a class="btn btn-sm btn-outline-primary" href="{{route('articles.edit', compact('article'))}}">Edit
                    article</a>
            </h3>
            <blockquote class="blockquote">
                <p class="mb-2" style="text-align: justify">
                    {{$article->content}}
                </p>
                <footer class="blockquote-footer">
                    Author: {{$article->user->name}}, created in
                    <cite title="Created at article">
                        {{$article->created_at->diffForHumans()}}
                    </cite>
                </footer>
            </blockquote>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3>
                Comments
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Add comment
                </button>
            </h3>
        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalScrollableTitle">Create new comment</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="comment-form">
                        <form id="create-comment">
                            @csrf
                            <input type="hidden" id="article_id" value="{{$article->id}}">
                            <div class="form-group">
                                <label for="commentFormControl">Comment</label>
                                <textarea name="body" class="form-control" id="commentFormControl" rows="3"
                                          required></textarea>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close-create-comment-modal" data-bs-dismiss="modal">Close</button>
                    <button type="button" id="create-comment-btn" class="btn btn-primary">Comment</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col scrollit">
            @foreach($article->comments as $comment)
                <x-comment-component :$comment />
            @endforeach
        </div>
    </div>

    <div class="row pt-1">
        <div class="col">
            {{$article->comments->links()}}
        </div>
    </div>

@endsection
