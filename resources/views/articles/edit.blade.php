@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h1>Edit article</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <form method="post" action="{{route('articles.update', ['article' => $article])}}">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="article-title">Title</label>
                    <input name="title" type="text" class="form-control" id="article-title" value="{{$article->title}}">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Content</label>
                    <textarea rows="3" name="content" class="form-control" id="exampleInputPassword1">{{$article->content}}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">Update article</button>
            </form>
        </div>
    </div>
    <div class="row">
        <div style="padding-top: 25px" class="col-2">
            <a href="{{route('articles.index')}}" class="btn btn-primary btn-sm active" role="button"
               aria-pressed="true">Back</a>
        </div>
    </div>

@endsection
