<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Comment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @param Article $article
     * @return JsonResponse
     */
    public function store(Request $request, Article $article): JsonResponse
    {
        $request->validate([
            'body' => 'required|min:5'
        ]);
        $comment = new Comment();
        $comment->user_id = $request->user()->id;
        $comment->body = $request->input('body');
        $comment->article_id = $article->id;
        $comment->save();
        return response()->json([
            'comment' => view('comments.comment', compact('comment'))->render(),
            'message' => 'Ваш комментарий будет добавлен после модерации администратором'
        ], 201);
    }

    /**
     * @param Article $article
     * @param Comment $comment
     * @return JsonResponse
     */
    function destroy(Article $article, Comment $comment): JsonResponse
    {
        $comment->delete();
        return response()->json([], 204);
    }
}
