<?php

namespace App\Policies;

use App\Models\Article;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param $ability
     * @return void|bool
     */
    public function before(User $user, $ability)
    {
        if ($user->hasRole(User::ADMIN)) {
            return true;
        }
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->approved;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Article $article
     * @return mixed
     */
    public function update(User $user, Article $article): bool
    {
        return $user->id === $article->user->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Article $article
     * @return mixed
     */
    public function delete(User $user, Article $article)
    {
        return $user->id === $article->user->id;
    }

    /**
     * @param User $user
     * @param Article $article
     * @return bool
     */
    public function edit(User $user, Article $article): bool
    {
        return $user->id === $article->user->id;
    }
}
