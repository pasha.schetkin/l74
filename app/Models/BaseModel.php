<?php

namespace App\Models;

use Closure;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Expression;

/**
 * Class BaseModel
 * @package App\Models
 *
 * @method static where(Closure|string|array|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and')
 * @method static firstOrCreate(array $state)
 * @method static whereHas(string $relation, Closure $callback = null, string $operator = '>=', int $count = 1)
 * @method static create(array $validate_data)
 * @method static find(mixed $id)
 * @method static paginate(int $int = null)
 * @method static orderBy(string $column, mixed $rule = 'asc')
 * @method static limit(int $limit)
 * @method static first()
 * @method static count()
 * @method static whereIn(string $string, array $toArray)
 * @method static chunk(int $count, callable $callback)
 * @method static findOrFail(int $id)
 * @method static firstOrNew(array $array)
 * @method static firstOrFail(array $columns = ['*'])
 * @method static orWhereNull(string|array $column)
 * @method static whereNotNull(string|array $columns, string $boolean = 'and')
 *
 * @property int id
 */
abstract class BaseModel extends Model
{
    use HasFactory;
}
