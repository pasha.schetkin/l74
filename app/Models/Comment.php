<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Comment
 * @package App\Models
 *
 * @property int id
 * @property User user
 * @property string text
 * @property Article article
 * @property bool approved
 */
class Comment extends BaseModel
{

    /**
     * @var string[]
     */
    protected $fillable = ['user_id', 'body', 'article_id', 'approved'];

    /**
     * @return BelongsTo
     */
    public function article(): BelongsTo
    {
        return $this->belongsTo(Article::class);
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
