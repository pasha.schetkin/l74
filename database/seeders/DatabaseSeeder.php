<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['Moderator', 'BlackList', 'Editor'];
        foreach ($roles as $role)
        {
            \App\Models\Role::factory()->state(['name' => $role])->create();
        }

        \App\Models\User::factory()->state(
            ['email' => 'admin@admin.com']
        )->has(
            \App\Models\Role::factory()->state(
                ['name' => 'Admin']
            )
        )->create();

        $roles = \App\Models\Role::where('name', '!=', 'Admin')->get();
        \App\Models\User::factory()
            ->count(3)
            ->create();

        \App\Models\User::all()->each(function ($user) use ($roles) {
            $user->roles()->attach(
                $roles->random(rand(1, 3))->pluck('id')->toArray()
            );
        });

        $users = \App\Models\User::all();
        foreach ($users as $user) {
            \App\Models\Article::factory()->count(rand(7, 9))->for($user)
                ->create();
        }

        $articles = \App\Models\Article::all();
        foreach ($articles as $article) {
            foreach ($users as $user) {
                \App\Models\Comment::factory()
                    ->count(rand(1, 3))
                    ->for($article)
                    ->for($user)
                    ->create();
            }
        }
    }
}
